﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.ComponentModel;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace DAL
{
    public class Data : IDisposable
    {
        /*-------------------------------------------------------------*/
        #region Dispose Implementation
        // Pointer to an external unmanaged resource.
        private IntPtr handle;
        // Other managed resource this class uses.
        private Component component = new Component();
        // Track whether Dispose has been called.
        private bool disposed = false;

        //Constructor
        public Data()
        {
            // this.handle = handle;
        }
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue 
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }


        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the 
        // runtime from inside the finalizer and you should not reference 
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    component.Dispose();
                }

                // Call the appropriate methods to clean up 
                // unmanaged resources here.
                // If disposing is false, 
                // only the following code is executed.
                CloseHandle(handle);
                handle = IntPtr.Zero;
            }
            disposed = true;
        }


        // Use interop to call the method necessary  
        // to clean up the unmanaged resource.
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method 
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        //MyResource()      
        //{
        // Do not re-create Dispose clean-up code here.
        // Calling Dispose(false) is optimal in terms of
        // readability and maintainability.
        // Dispose(false);
        //}

        ~Data()
        {
            //	Dispose(false);
        }

        #endregion
        /*-------------------------------------------------------------*/

        /*-------------------------------------------------------------*/
        #region Declarations

        string sserver;
        string sdatabase;
        string susername;
        string spassword;
        string sConnectionString;

        #endregion
        /*-------------------------------------------------------------*/
        /*-------------------------------------------------------------*/
        #region Properties

        public string Server
        {
            get
            {
                return sserver;
            }
            set
            {
                sserver = value;
            }
        }
        public string Database
        {
            get
            {
                return sdatabase;
            }
            set
            {
                sdatabase = value;
            }
        }
        public string UserName
        {
            get
            {
                return susername;
            }
            set
            {
                susername = value;
            }
        }
        public string Password
        {
            get
            {
                return spassword;
            }
            set
            {
                spassword = value;
            }
        }
        public string ConnectionString
        {
            get
            {
                return sConnectionString;
            }
            set
            {
                sConnectionString = value;
            }
        }

        #endregion
        /*-------------------------------------------------------------*/

        /*-------------------------------------------------------------*/
        #region Private Methods
        /*
		Name: Procedure  CreateConnectionString
		Description: Concatinate Data class properties to create a connection string
					and evaluates if the connection string is valid or not.
		Returns: true if the connection string is valid, otherwise it returns false
		 */
        private bool CreateConnectionString()
        {

            try
            {
                /*
                sConnectionString = "Server=" +  this.Server  + ";" +
                                    "Initial Catalog=" + this.Database  + ";" +
                                    "User ID=" + this.UserName  + ";" +
                                    "Connect Timeout=30" + ";" +
                                    "Password=" + this.Password  + ";";
                */
                // sConnectionString = "Persist Security Info=False;Integrated Security=SSPI; Initial Catalog=WebServerXfer; server=CRMSVR; Connect Timeout=30";

                /*
                sConnectionString = "PWD=" + this.Password + ";" +
                                    "DSN=" + this.Server + ";" +
                                    "APP=Microsoft® Visual Studio .NET;" +
                                    "AnsiNPW=No;" +
                                    "AutoTranslate=No;" +
                                    "QuotedId=No;" +
                                    "UID=" + this.UserName + "";
                */

                if (this.IsConnectionValid() == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }

        }

        #endregion
        /*-------------------------------------------------------------*/

        /*-------------------------------------------------------------*/
        #region Public Methods

        #region "InitDataLayerObject"

        /*
		Name: Procedure InitDataLayerObject
		Description: Retrieves default configurations in the app.config file,
					creates the connection string then determines if the connection is valid
		Returns: true if the connection string is valid, otherwise it returns false
		Overload: true
		 */
        public bool InitDataLayerObject()
        {
            sserver = ConfigurationManager.AppSettings["host"].ToString();
            sdatabase = ConfigurationManager.AppSettings["dbname"].ToString();
            susername = ConfigurationManager.AppSettings["username"].ToString();
            spassword = ConfigurationManager.AppSettings["password"].ToString();

            //sConnectionString = ConfigurationManager.AppSettings["constr"].ToString();

            sConnectionString = "Server=" + sserver + ";Database=" + sdatabase + ";User ID=" + susername + ";Password=" + spassword + ";Persist Security info=False;Enlist=false; Pooling=false;Max Pool Size=100;Connect Timeout=300";

            if (this.CreateConnectionString() != true)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public bool InitDataLayerObject(string ConnectionString)
        {
            sserver = ConfigurationManager.AppSettings["server"].ToString();
            sdatabase = ConfigurationManager.AppSettings["database"].ToString();
            susername = ConfigurationManager.AppSettings["UserID"].ToString();
            spassword = ConfigurationManager.AppSettings["password"].ToString();

            sConnectionString = ConnectionString;

            if (this.CreateConnectionString() != true)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        /*
        Name: Procedure InitDataLayerObject
        Description: Creates the connection string from the parameters given then determines if the connection is valid
        Returns: true if the connection string is valid, otherwise it returns false
        Overload: true
         */
        public bool InitDataLayerObject(string psServer, string psDatabase, string psUserName, string psPassword)
        {

            sserver = psServer;
            sdatabase = psDatabase;
            susername = psUserName;
            spassword = psPassword;

            if (this.CreateConnectionString() != true)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        #endregion


        #region "IsConnectionValid"

        /*
		Name: Procedure IsConnectionValid
		Description: Determines if the created connection string is valid
		Returns: true if the connection string is valid, otherwise it returns false
		Overload: true
		 */
        public bool IsConnectionValid()
        {
            SqlConnection oConn = new SqlConnection();

            try
            {
                oConn.ConnectionString = sConnectionString;
                oConn.Open();
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                oConn.Close();
                oConn.Dispose();
                oConn = null;
            }

        }

        /*
        Name: Procedure IsConnectionValid
        Description: Determines if the parameter connection string is valid
        Returns: true if the connection string is valid, otherwise it returns false
        Overload: true
         */
        public bool IsConnectionValid(string p)
        {
            SqlConnection oConn = new SqlConnection();

            try
            {
                oConn.ConnectionString = p;
                oConn.Open();
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                oConn.Close();
                oConn.Dispose();
            }

        }

        #endregion


        #region ExecuteNonQuery

        public int ExecuteNonQuery(string sQuery)
        {
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, System.Data.CommandType.Text, sQuery);
        }

        public int ExecuteNonQuery(string sQuery, params SqlParameter[] commandParameters)
        {
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, System.Data.CommandType.StoredProcedure, sQuery, commandParameters);
        }

        public DataSet ExecuteQuery(string sQuery)
        {
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(this.ConnectionString, System.Data.CommandType.Text, sQuery);
            return ds;
        }

        public DataSet ExecuteQuery(string sStoredProcedure, params SqlParameter[] commandParameters)
        {
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(this.ConnectionString, System.Data.CommandType.StoredProcedure, sStoredProcedure, commandParameters);
            return ds;
        }

        public object ExecuteNonScalar(string sQuery)
        {
            return SqlHelper.ExecuteScalar(this.ConnectionString, System.Data.CommandType.Text, sQuery);
        }


        #endregion

        #endregion
    }
}
