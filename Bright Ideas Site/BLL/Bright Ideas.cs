﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;

namespace BLL
{
    public class Bright_Ideas
    {
        DAL.Data data = new DAL.Data();

        public DataSet GetApprovalStepStatusRole(int StatusId, string UserNm)
        {
            DataSet ds = new DataSet();
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter("@idea",StatusId),
                new SqlParameter("@userNm",UserNm),
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spGetApprovalStepStatusRole", sqlParam);
            }
            return ds;
        }

        public DataSet GetRequestorInformation(string username)
        {
            DataSet ds = new DataSet();
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter("@userNm",username)
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spSelectRequestorInformation", sqlParam);
            }
            return ds;
        }

        public DataSet GetAceChampion(string businessUnit)
        {
            DataSet ds = new DataSet();
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter("@BusinessUnit",businessUnit)
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spSelectAceChampion", sqlParam);
            }
            return ds;
        }

        public DataSet GetProjectOwner(string businessUnit)
        {
            DataSet ds = new DataSet();
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter("@BusinessUnit",businessUnit)
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spSelectProjectOwner", sqlParam);
            }
            return ds;
        }

        public DataSet GetBusinessUnitList()
        {
            DataSet ds = new DataSet();
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spSelectBusinessUnit");
            }
            return ds;
        }

        public DataSet SaveIdea(string authorUserNm, string ideaTitle, string ideaType, string estimatedBenefits, string processChampion, string projectOwner, int status, string estimatedCompletionDateTime)
        {
            DataSet ds = new DataSet();

            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter(@"IdeaAuthorUserNm",authorUserNm),
                new SqlParameter(@"IdeaTitle",ideaTitle),
                new SqlParameter(@"IdeaType",ideaType),
                new SqlParameter(@"EstimatedBenefits",estimatedBenefits),
                new SqlParameter(@"ProcessChampion",processChampion),
                new SqlParameter(@"ProjectOwnerUserNm",projectOwner),
                new SqlParameter(@"Status",status),
                new SqlParameter(@"ECDTTM",estimatedCompletionDateTime)
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spSaveIdea", sqlParam);
            }
            return ds;
        }

        public DataSet DownloadFile(int id, int type)
        {
            DataSet ds = new DataSet();
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter(@"IdeaReferenceId",id),
                new SqlParameter(@"AttachmentFileType",type)
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spGetAttachment", sqlParam);
            }
            return ds;
        }

        public DataSet GetIdeaDetails(int id)
        {
            DataSet ds = new DataSet();
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter(@"id",id)
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spSelectIdea", sqlParam);
            }
            return ds;
        }

        public void UploadAttachment(int id, string name, string contenttype, string type, byte[] file)
        {
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter(@"IdeaReferenceId",id),
                new SqlParameter(@"AttachmentFileNm",name),
                new SqlParameter(@"AttachmentContentType",contenttype),
                new SqlParameter(@"AttachmentType",type),
                new SqlParameter(@"Data",file)
            };
            if (data.InitDataLayerObject() == true)
            {
                data.ExecuteQuery("dbo.spSaveAttachment", sqlParam);
            }
        }

        public DataSet SelectAllIdeas()
        {
            DataSet ds = new DataSet();
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spSelectAllIdeas");
            }
            return ds;
        }

        public DataSet SelectStatus(int id)
        {
            DataSet ds = new DataSet();
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter(@"id",id)
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spSelectStatus", sqlParam);
            }
            return ds;
        }

        public DataSet SelectSupervisors(string username)
        {
            DataSet ds = new DataSet();
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter(@"username",username)
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spSelectSupervisors", sqlParam);
            }
            return ds;
        }

        public DataSet GetBrightIdeaTypes()
        {
            DataSet ds = new DataSet();
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spGetBrightIdeaTypes");
            }
            return ds;
        }

        public DataSet GetIdeaTypes()
        {
            DataSet ds = new DataSet();
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spGetIdeaTypes");
            }
            return ds;
        }

        public DataSet SelectProcessChampions(string POUsername, string ACEUsername)
        {
            DataSet ds = new DataSet();
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter(@"POUsername",POUsername),
                new SqlParameter(@"ACEUsername",ACEUsername)
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spSelectProcessChampions", sqlParam);
            }
            return ds;
        }

        public DataSet UpdateIdea(int id, string actualBenefits, int status, string actualCompletionDttm, string BIType)
        {
            DataSet ds = new DataSet();
            SqlParameter[] sqlParam = new SqlParameter[]
            {
                new SqlParameter(@"id",id),
                new SqlParameter(@"ActualBenefits",actualBenefits),
                new SqlParameter(@"StatusId",status),
                new SqlParameter(@"ActualCompletionDate",actualCompletionDttm),
                new SqlParameter(@"BrightIdeaType",BIType),
                new SqlParameter(@"ActionId",1), //1 palang lagi
                new SqlParameter(@"commenttxt",1), //1 palang lagi
                new SqlParameter(@"approverusernm",Environment.UserName)
            };
            if (data.InitDataLayerObject() == true)
            {
                ds = data.ExecuteQuery("dbo.spUpdateIdea", sqlParam);
            }
            return ds;
        }
    }
}
