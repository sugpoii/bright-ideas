﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Bright_Ideas_Site.Startup))]
namespace Bright_Ideas_Site
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
