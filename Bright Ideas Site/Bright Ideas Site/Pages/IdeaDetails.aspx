﻿<%@ Page Title="Idea Details" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="IdeaDetails.aspx.cs" Inherits="Bright_Ideas_Site.Pages.ApprovalView" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="CreateDttm" runat="server" ClientIDMode="Static" />
    <div class="col-md-10 col-md-offset-1">
        <h2><asp:Label ID="lblHeader" runat="server" Text="Header"></asp:Label></h2>
    </div>
    <%--    Status--%>
    <div class="col-md-10 col-md-offset-1 parent-div" style="text-align: center">
        <h3><asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
        </h3>
    </div>
    <%--    Requestor Information--%>
    <div class="col-md-10 col-md-offset-1 parent-div">
        <div class="row">
            <div class="col-md-11">
                <h4 style="float: left;">Requestor Information</h4>
            </div>
            <div class="col-md-1 minimize">
                <strong><a data-toggle="collapse" data-target="#divRequestorInformation">_</a></strong>
            </div>
        </div>
        <div class="collapse in" id="divRequestorInformation">
            <hr />
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtRequestorName">Requestor Name</label>
                    <asp:TextBox ID="txtRequestorName" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label for="txtPosition">Position</label>
                    <asp:TextBox ID="txtPosition" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtRankLevel">Rank/Level</label>
                    <asp:TextBox ID="txtRankLevel" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label for="txtBusinessUnit">Business Unit</label>
                    <asp:TextBox ID="txtBusinessUnit" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtACEChampion">ACE Champion</label>
                    <asp:TextBox ID="txtACEChampion" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label for="txtLineManager">Line Manager</label>
                    <asp:TextBox ID="txtLineManager" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtBusinessHead">Business Head</label>
                    <asp:TextBox ID="txtBusinessHead" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <%--    Idea Details--%>
    <div class="col-md-10 col-md-offset-1 parent-div">
        <div class="row">
            <div class="col-md-11">
                <h4 style="float: left;">Bright Idea Details</h4>
            </div>
            <div class="col-md-1 minimize">
                <strong><a data-toggle="collapse" data-target="#divDetails">_</a></strong>
            </div>
        </div>
        <div class="collapse in" id="divDetails">
            <hr />
            <div class="form-group">
                <label for="txtIdeaTitle">Idea Title</label>
                <asp:TextBox ID="txtIdeaTitle" class="form-control" type="text" disabled runat="server"></asp:TextBox>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    <label for="txtIdeaType">Idea Type</label>
                    <asp:TextBox ID="txtIdeaType" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
                <div class="col-md-4">
                    <label for="ddlBrightIdeaType">Bright Idea Type</label>
                    <asp:DropDownList ID="ddlBrightIdeaType" class="form-control" runat="server">
                        <asp:ListItem Enabled="true" Text="" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBrightIdeaType" disabled class="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="col-md-4">
                    <label for="txtIdeaStatus">Idea Status</label>
                    <asp:TextBox ID="txtIdeaStatus" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="btnCurrentState">Current State</label><br />
                    <asp:Button ID="btnCurrentState" runat="server" class="btn btn-default btn-sm btn-block" Text="Download Attachment" OnClick="btnCurrentState_Click" />
                </div>
                <div class="col-md-6">
                    <label for="btnProposedImprovement">Proposed Improvement</label><br />
                    <asp:Button ID="btnProposedImprovement" runat="server" class="btn btn-default btn-sm btn-block" Text="Download Attachment" OnClick="btnProposedImprovement_Click" />
                </div>
            </div>
            <div class="form-group">
                <label for="txtEstimatedBenefits">Estimated Benefits</label>
                <asp:TextBox ID="txtEstimatedBenefits" TextMode="multiline" class="form-control" disabled type="text" runat="server"></asp:TextBox>
            </div>
            <div class="form-group">
                <label for="txtActualBenefits">Actual Benefits</label>
                <asp:TextBox ID="txtActualBenefits" TextMode="multiline" class="form-control" required type="text" runat="server"></asp:TextBox>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtEstimatedCompletionDate">Estimated Completion Date</label>
                    <asp:TextBox ID="txtEstimatedCompletionDate" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label for="txtActualCompletionDate">Actual Completion Date</label>
                    <asp:TextBox ID="txtActualCompletionDate" class="form-control" required type="text" runat="server" ClientIDMode="Static"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <%--    Process Champion--%>
    <div class="col-md-10 col-md-offset-1 parent-div">
        <div class="row">
            <div class="col-md-11">
                <h4 style="float: left;">Process Champion</h4>
            </div>
            <div class="col-md-1 minimize">
                <strong><a data-toggle="collapse" data-target="#divProcessChampion">_</a></strong>
            </div>
        </div>
        <div class="collapse in" id="divProcessChampion">
            <hr />
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtBusinessUnit">Business Unit</label>
                    <asp:TextBox ID="txtChampionBusinessUnit" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label for="txtACEChampion">ACE Champion</label>
                    <asp:TextBox ID="txtChampionACEChampion" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtChampionLineManager">Line Manager</label>
                    <asp:TextBox ID="txtChampionLineManager" TextMode="DateTimeLocal" class="form-control" disabled runat="server"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label for="txtChampionBusinessHead">Business Head</label>
                    <asp:TextBox ID="txtChampionBusinessHead" TextMode="DateTimeLocal" class="form-control" disabled runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtProjectOwner">Project Owner</label>
                    <asp:TextBox ID="txtProjectOwner" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="box-body">
                    <div class="row">
                        <div class="form-group" style="padding-left: 20px; padding-right: 20px">
                            <table style="border-left: solid 1px #0094ff; border-right: solid 1px #0094ff; border-top: solid 1px #0094ff; background-color: lightgray; width: 100%; margin: 0; padding: 5px; font-size: 8pt;">

                                <tr>
                                    <th class="text-center" scope="col" style="width: 20%;">Matrix</th>
                                    <th class="text-center" scope="col" style="width: 20%;">Approver`s Action</th>
                                    <th class="text-center" scope="col" style="width: 20%;">Approver</th>
                                    <th class="text-center" scope="col" style="width: 15%;">Date</th>
                                    <th class="text-center" scope="col" style="width: 20%;">Remarks</th>
                                    <th class="text-center" scope="col" style="width: 5%;">Age</th>
                                </tr>

                            </table>
                            <asp:GridView ID="grdApprovalProcess" runat="server" AutoGenerateColumns="False" BorderStyle="None" CssClass="table table-bordered table-condensed" RowStyle-Wrap="true" EmptyDataText="No approval yet." ShowFooter="false" ShowHeader="false" ShowHeaderWhenEmpty="True" DataSourceID="SqlDataSource1">
                                <Columns>
                                    <asp:BoundField DataField="ApprovalStepDsca" HeaderText="Matrix" SortExpression="ApprovalStepDsca" ItemStyle-Width="20%" HeaderStyle-Width="20%" />
                                    <asp:BoundField DataField="StatusDsca" HeaderText="Approver`s Action" SortExpression="StatusDsca" ItemStyle-Width="20%" HeaderStyle-Width="20%" />
                                    <asp:BoundField DataField="fullName" HeaderText="fullName" SortExpression="fullName" ItemStyle-Width="20%" HeaderStyle-Width="20%" />
                                    <asp:BoundField DataField="CreateDttm" HeaderText="Date" SortExpression="CreateDttm" ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                    <asp:BoundField DataField="CommentTxt" HeaderText="Remarks" ItemStyle-Wrap="true" SortExpression="CommentTxt" ItemStyle-Width="20%" HeaderStyle-Width="20%" />
                                    <asp:BoundField DataField="cycleDays" HeaderText="Age" ReadOnly="True" SortExpression="cycleDays" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                </Columns>
                                <RowStyle Wrap="True" />
                            </asp:GridView>
                        </div>


                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NWSERVER %>" SelectCommand="[dbo].[spGetApprovalMatrix]" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="iReferenceId" QueryStringField="id" />
                            </SelectParameters>
                        </asp:SqlDataSource>

                    </div>
                </div>

    <%--    Buttons--%>
    <div class="col-md-10 col-md-offset-1" style="text-align: right;">
        <asp:Button ID="btnReject" runat="server" class="btn btn-danger btn-sm" Style="margin: 10px;" Text="Reject" OnClick="btnReject_Click" />
        <asp:Button ID="btnUpdate" runat="server" class="btn btn-success btn-sm" Style="margin: 10px;" Text="Update" OnClick="btnUpdate_Click" />
        <asp:Button ID="btnApprove" runat="server" class="btn btn-success btn-sm" Style="margin: 10px;" Text="Approve" OnClick="btnApprove_Click" />
        <asp:Button ID="btnImplemented" runat="server" class="btn btn-success btn-sm" Style="margin: 10px;" Text="Implemented" OnClick="btnImplemented_Click" />
        <asp:Button ID="btnConfirmed" runat="server" class="btn btn-success btn-sm" Style="margin: 10px;" Text="Confirmed" OnClick="btnConfirmed_Click"/>
        <asp:Button ID="btnCompleteRewarded" runat="server" class="btn btn-success btn-sm" Style="margin: 10px;" Text="Rewarded" OnClick="btnCompleteRewarded_Click" />
        <asp:Button ID="btnExit" runat="server" class="btn btn-warning btn-sm" Style="margin: 10px;" Text="Exit" OnClick="btnExit_Click" />
    </div>


    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#txtActualCompletionDate").datepicker();
        });
    </script>
    <%--<script>
        function Validation() {
            var act_date = $('#txtActualCompletionDate').val();
            var crt_date = $('#CreateDttm').val();
            if (new Date(act_date).getTime() > new Date().getTime() || new Date(act_date).getTime() < new Date(crt_date).getTime()) {
                $('#txtActualCompletionDate').focus();
                return false;
            }
            else {
                return true;
            }
        }
    </script>--%>
</asp:Content>
<%--  --%>