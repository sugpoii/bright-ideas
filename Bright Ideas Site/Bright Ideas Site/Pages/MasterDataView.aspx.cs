﻿using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace Bright_Ideas_Site.Pages
{
    public partial class MasterDataView : System.Web.UI.Page
    {
        BLL.Bright_Ideas brightIdeas = new BLL.Bright_Ideas();

        protected void Page_Load(object sender, EventArgs e)
        {
            Display.Visible = false;
            PopulateGridView();
        }

        public void PopulateGridView()
        {
            DataSet ds = new DataSet();
            ds = brightIdeas.SelectAllIdeas();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdIdeas.DataSource = ds;
                grdIdeas.DataBind();
                SetHeaders();
            }
            else
            {
                Display.Visible = true;
            }
        }

        protected void grdIdeas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Text = "<a href='\\Pages\\IdeaDetails.aspx?id="+ e.Row.Cells[0].Text + "'>" + e.Row.Cells[1].Text + "</a>";
            }
        }


        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdIdeas.PageIndex = e.NewPageIndex;
            grdIdeas.DataBind();
            SetHeaders();
        }

        public void SetHeaders()
        {
            grdIdeas.HeaderRow.Cells[0].Text = "Idea No.";
            grdIdeas.HeaderRow.Cells[1].Text = "Idea Title";
            grdIdeas.HeaderRow.Cells[2].Text = "Estimated Completion Date";
            grdIdeas.HeaderRow.Cells[3].Text = "Actual Completion Date";
            grdIdeas.HeaderRow.Cells[4].Text = "Idea Type";
            grdIdeas.HeaderRow.Cells[5].Text = "Bright Idea Type";
            grdIdeas.HeaderRow.Cells[6].Text = "Status";
            grdIdeas.HeaderRow.Cells[7].Text = "Idea Author";
        }
    }
}