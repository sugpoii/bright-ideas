﻿<%@ Page Title="Master Data View" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MasterDataView.aspx.cs" Inherits="Bright_Ideas_Site.Pages.MasterDataView" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        body {
            background-color: white;
        }
    </style>
    <div class="col-md-12">
        <h2>Master Data View</h2>
        <asp:GridView ID="grdIdeas" runat="server" BackColor="White" BorderColor="#555555" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" GridLines="Horizontal" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="20" OnRowDataBound="grdIdeas_RowDataBound">
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <HeaderStyle BackColor="#555555" Font-Bold="True" ForeColor="White" />
            <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="#555555" />
            <RowStyle BackColor="White" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#555555" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#487575" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#275353" />
        </asp:GridView>
        <br />
        <asp:Label ID="Display" runat="server" Text="No data to display."></asp:Label>
    </div>
</asp:Content>
