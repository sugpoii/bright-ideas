﻿<%@ Page Title="Submit A Bright Idea" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LoggingSheet.aspx.cs" Inherits="Bright_Ideas_Site.Pages.LoggingView" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-10 col-md-offset-1">
        <h2>Submit A Bright Idea</h2>
    </div>
    <div class="col-md-10 col-md-offset-1 parent-div">
        <div class="row">
            <div class="col-md-11">
                <h4 style="float: left;">Requestor Information</h4>
            </div>
            <div class="col-md-1 minimize">
                <strong><a data-toggle="collapse" data-target="#divRequestorInformation">_</a></strong>
            </div>
        </div>
        <div class="collapse in" id="divRequestorInformation">
            <hr />
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtRequestorName">Requestor Name</label>
                    <asp:TextBox ID="txtRequestorName" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label for="txtPosition">Position</label>
                    <asp:TextBox ID="txtPosition" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtRankLevel">Rank/Level</label>
                    <asp:TextBox ID="txtRankLevel" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label for="txtBusinessUnit">Business Unit</label>
                    <asp:TextBox ID="txtBusinessUnit" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtACEChampion">ACE Champion/s</label>
                    <asp:TextBox ID="txtACEChampion" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label for="txtLineManager">Line Manager</label>
                    <asp:TextBox ID="txtLineManager" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtBusinessHead">Business Head</label>
                    <asp:TextBox ID="txtBusinessHead" class="form-control" type="text" disabled runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-10 col-md-offset-1 parent-div">
        <div class="row">
            <div class="col-md-11">
                <h4 style="float: left;">Bright Idea Details</h4>
            </div>
            <div class="col-md-1 minimize">
                <strong><a data-toggle="collapse" data-target="#divDetails">_</a></strong>
            </div>
        </div>
        <div class="collapse in" id="divDetails">
            <hr />
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtIdeaTitle">Idea Title</label>
                    <asp:TextBox ID="txtIdeaTitle" class="form-control" type="text" runat="server" required></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label for="ddlIdeaType">Idea Type</label>
                    <asp:DropDownList ID="ddlIdeaType" class="form-control" runat="server" required
                        AppendDataBoundItems="true">
                        <asp:ListItem Enabled="true" Text="Select Idea Type" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="fuploadCurrentState">Current State</label>
                    <asp:FileUpload ID="fuploadCurrentState" runat="server" required />
                </div>
                <div class="col-md-6">
                    <label for="fuploadProposedImprovement">Proposed Improvement</label>
                    <asp:FileUpload ID="fuploadProposedImprovement" runat="server" required />
                </div>
            </div>
            <div class="form-group">
                <label for="txtEstimatedBenefits">Estimated Benefits</label>
                <asp:TextBox ID="txtEstimatedBenefits" TextMode="multiline" class="form-control" type="text" runat="server" required></asp:TextBox>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="txtEstimatedCompletionDate">Estimated Completion Date</label>
                    <asp:TextBox ID="txtEstimatedCompletionDate" ClientIDMode="Static" class="form-control" type="text" runat="server" required></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <fieldset>
                <div class="col-md-10 col-md-offset-1 parent-div">
                    <div class="row">
                        <div class="col-md-11">
                            <h4 style="float: left;">Process Champion</h4>
                        </div>
                        <div class="col-md-1 minimize">
                            <strong><a data-toggle="collapse" data-target="#divProcessChampion">_</a></strong>
                        </div>
                    </div>
                    <div class="collapse in" id="divProcessChampion">
                        <hr />
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="ddlBusinessUnit">Business Unit</label>
                                <asp:DropDownList ID="ddlBusinessUnit" class="form-control" runat="server" required
                                    AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged">
                                    <asp:ListItem Enabled="true" Text="Select Business Unit" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <label for="ddlProjectOwner">Project Owner</label>
                                <asp:DropDownList ID="ddlProjectOwner" class="form-control" runat="server"
                                    AppendDataBoundItems="false" AutoPostBack="true">
                                    <asp:ListItem Enabled="true" Text="Select Project Owner" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="ddlACEChampion">ACE Champion</label>
                                <asp:DropDownList ID="ddlACEChampion" class="form-control" runat="server" required
                                    AppendDataBoundItems="false" AutoPostBack="true" OnSelectedIndexChanged="ddlACEChampion_SelectedIndexChanged">
                                    <asp:ListItem Enabled="true" Text="Select ACE Champion" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <label for="txtChampionLineManager">Line Manager</label>
                                <asp:TextBox ID="txtChampionLineManager" TextMode="DateTimeLocal" class="form-control" disabled runat="server"></asp:TextBox>
                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="txtChampionBusinessHead">Business Head</label>
                                <asp:TextBox ID="txtChampionBusinessHead" TextMode="DateTimeLocal" class="form-control" disabled runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="col-md-10 col-md-offset-1" style="text-align: right;">
        <asp:Button ID="btnCancel" runat="server" class="btn btn-danger btn-sm" Style="margin: 10px;" Text="Cancel" OnClick="btnCancel_Click" />
        <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary btn-sm" Style="margin: 10px;" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="javascript:return Validation();" />
    </div>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#txtEstimatedCompletionDate").datepicker();
        });
    </script>
    <script>
        function Validation() {
            var bu = "<%=ddlBusinessUnit.ClientID %>";
            var po = "<%=ddlProjectOwner.ClientID %>";
            var it = "<%=ddlIdeaType.ClientID %>";
            var ac = "<%=ddlACEChampion.ClientID %>";
            if (document.getElementById(it).selectedIndex == 0) {
                document.getElementById(it).focus();
                return false;
            }
            if (document.getElementById(bu).selectedIndex == 0) {
                document.getElementById(bu).focus();
                return false;
            }
            if (document.getElementById(po).selectedIndex == 0) {
                document.getElementById(po).focus();
                return false;
            }
            if (document.getElementById(ac).selectedIndex == 0) {
                document.getElementById(ac).focus();
                return false;
            }
            var estDate = $('#txtEstimatedCompletionDate').datepicker('getDate', '+1d');
            var todaysDate = new Date();
            if (estDate.setHours(0, 0, 0, 0) < todaysDate.setHours(0, 0, 0, 0)) {
                return false;
            }
        return true;
        }
    </script>
</asp:Content>
