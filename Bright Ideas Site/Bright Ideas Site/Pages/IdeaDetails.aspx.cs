﻿using System;
using System.Web.UI;
using System.Data;
using BLL;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;


namespace Bright_Ideas_Site.Pages
{
    public partial class ApprovalView : System.Web.UI.Page
    {
        BLL.Bright_Ideas brightIdeas = new BLL.Bright_Ideas();

        string status = "";
        int id;
        string actualCompletionDate = "";
        string actualBenefits = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Request.QueryString["id"] == null)
            {
                Page.Response.Redirect("MasterDataView.aspx");
            }
            txtActualBenefits.Enabled = false;
            txtActualBenefits.CssClass = "form-control";
            txtActualCompletionDate.Enabled = false;
            txtActualCompletionDate.CssClass = "form-control";
            ddlBrightIdeaType.Enabled = false;
            ddlBrightIdeaType.Visible = false;
            txtBrightIdeaType.Visible = false;
            btnApprove.Visible = false;
            btnCompleteRewarded.Visible = false;
            btnConfirmed.Visible = false;
            btnImplemented.Visible = false;
            btnReject.Visible = false;
            btnUpdate.Visible = false;
            GetData();
            StatusChecker();
        }

        public void GetData()
        {
            id = Convert.ToInt32(Page.Request.QueryString["id"]);
            DataSet ds = new DataSet();
            ds = brightIdeas.GetIdeaDetails(id);
            if (ds.Tables[0].Rows.Count == 0)
            {
                Page.Response.Redirect("MasterDataView.aspx");
            }
            CreateDttm.Value = ds.Tables[0].Rows[0]["CreateDttm"].ToString();
            status = ds.Tables[0].Rows[0]["StatusId"].ToString();
            SetStatus(status, ds.Tables[1].Rows[0]["BrightIdeaType"].ToString());
            PopulateRequestorsInformation(ds.Tables[0].Rows[0]["IdeaAuthorUserNm"].ToString());
            txtIdeaTitle.Text = ds.Tables[0].Rows[0]["IdeaTitle"].ToString();
            txtIdeaType.Text = ds.Tables[0].Rows[0]["DecodeTxt"].ToString();
            txtEstimatedBenefits.Text = ds.Tables[0].Rows[0]["EstimatedBenefits"].ToString();
            actualBenefits = ds.Tables[0].Rows[0]["ActualBenefits"].ToString();
            txtEstimatedCompletionDate.Text = ds.Tables[0].Rows[0]["EstimatedCompletionDttm"].ToString();
            actualCompletionDate = ds.Tables[0].Rows[0]["ActualCompletionDttm"].ToString();
            PopulateProcessChampions(ds.Tables[0].Rows[0]["ProjectOwnerUserNm"].ToString(), ds.Tables[0].Rows[0]["ProcessChampionAceChampionUserNm"].ToString());
            ds = brightIdeas.GetApprovalStepStatusRole(id, /*Environment.UserName*/ "rtayamora");
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblHeader.Text = "";
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (row["StatusDsca"].ToString() == "Update")
                    {
                        ddlBrightIdeaType.Enabled = true;
                        ddlBrightIdeaType.Visible = true;
                        PopulateBrightIdeaType();
                        btnUpdate.Visible = true;
                    }
                    if (row["StatusDsca"].ToString() == "Approve")
                    {
                        btnApprove.Visible = true;
                    }
                    if (row["StatusDsca"].ToString() == "Reject")
                    {
                        btnReject.Visible = true;
                    }
                    if (row["StatusDsca"].ToString() == "Confirm")
                    {
                        txtActualBenefits.Enabled = true;
                        txtActualCompletionDate.Enabled = true;
                        btnConfirmed.Visible = true;
                    }
                    if (row["StatusDsca"].ToString() == "Implement")
                    {
                        btnImplemented.Visible = true;
                    }
                    if (row["StatusDsca"].ToString() == "Complete")
                    {
                        btnCompleteRewarded.Visible = true;
                    }
                }
            }
            else
            {
                lblHeader.Text = "Idea Details";
            }
        }
        public void SetStatus(string id, string type)
        {
            DataSet ds = new DataSet();
            ds = brightIdeas.SelectStatus(Convert.ToInt32(id));
            lblStatus.Text = ds.Tables[0].Rows[0]["ApprovalStepDsca"].ToString();
            txtIdeaStatus.Text = ds.Tables[0].Rows[0]["ApprovalStepDsca"].ToString();
            txtBrightIdeaType.Text = type;
        }
        public void PopulateBrightIdeaType()
        {
            if (!IsPostBack)
            {
                ArrayList list = new ArrayList();
                DataSet ds = brightIdeas.GetBrightIdeaTypes();
                ddlBrightIdeaType.DataSource = ds.Tables[0];
                ddlBrightIdeaType.DataTextField = "DecodeTxt";
                ddlBrightIdeaType.DataValueField = "CodeTxt";
                ddlBrightIdeaType.DataBind();
            }
        }
        public void PopulateProcessChampions(string projectOwner, string aceChampion)
        {
            DataSet ds = new DataSet();
            string ff = projectOwner.Replace("CCAC\\", "");
            ds = brightIdeas.SelectProcessChampions(ff, aceChampion.Replace("CCAC\\", ""));
            txtChampionACEChampion.Text = ds.Tables[0].Rows[0]["AceChampion"].ToString();
            txtChampionBusinessHead.Text = ds.Tables[0].Rows[0]["BUHead"].ToString();
            txtChampionBusinessUnit.Text = ds.Tables[0].Rows[0]["BU"].ToString();
            txtChampionLineManager.Text = ds.Tables[0].Rows[0]["LineManager"].ToString();
            txtProjectOwner.Text = ds.Tables[0].Rows[0]["ProjectOwner"].ToString();
        }
        public void PopulateRequestorsInformation(string username)
        {
            DataSet ds = new DataSet();
            ds = brightIdeas.GetRequestorInformation(username);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtRequestorName.Text = ds.Tables[0].Rows[0]["FULL_NAME"].ToString();
                txtPosition.Text = ds.Tables[0].Rows[0]["POSITION"].ToString();
                txtRankLevel.Text = ds.Tables[0].Rows[0]["RANK_LEVEL"].ToString();
                txtBusinessUnit.Text = ds.Tables[0].Rows[0]["BU"].ToString();
                ds = brightIdeas.GetAceChampion(ds.Tables[0].Rows[0]["BU"].ToString());
                string names = "";
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    names = names + item["FULL_NAME"].ToString() + "; ";
                }
                txtACEChampion.Text = names;
                ds = brightIdeas.SelectSupervisors(username);
                txtLineManager.Text = ds.Tables[0].Rows[0]["LineManager"].ToString();
                txtBusinessHead.Text = ds.Tables[0].Rows[0]["BUHead"].ToString();
            }
        }

        public void StatusChecker()
        {
            if (status != "1")
            {
                txtBrightIdeaType.Visible = true;
            }
            if (status == "7")
            {
                txtActualCompletionDate.Text = actualCompletionDate;
                txtActualBenefits.Text = actualBenefits;
            }
            if (status == "10")
            {
                txtActualCompletionDate.Text = actualCompletionDate;
                txtActualBenefits.Text = actualBenefits;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (status == "1")
            {
                DataSet ds = new DataSet();
                ds = brightIdeas.UpdateIdea(id,"",2,"",ddlBrightIdeaType.SelectedValue);
            }
            Response.Redirect(Request.RawUrl);
        }
        protected void btnApprove_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            if (status == "2")
            {
                //check if idea is quick win or project
                if (txtBrightIdeaType.Text == "Project")
                {
                    ds = brightIdeas.UpdateIdea(id, "", 3, "", "");
                }
                else
                {
                    ds = brightIdeas.UpdateIdea(id, "", 5, "", "");
                }
            }
            if (status == "3")
            {
                ds = brightIdeas.UpdateIdea(id, "", 4, "", "");
            }
            Response.Redirect(Request.RawUrl);
        }
        protected void btnImplemented_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            if (status == "4" || status == "5")
            {
                ds = brightIdeas.UpdateIdea(id, "", 6, "", "");
            }
            Response.Redirect(Request.RawUrl);
        }
        protected void btnConfirmed_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            if (status == "6")
            {
                ds = brightIdeas.UpdateIdea(id, txtActualBenefits.Text, 7, txtActualCompletionDate.Text, "");
            }
            Response.Redirect(Request.RawUrl);
        }
        protected void btnCompleteRewarded_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            if (status == "7")
            {
                ds = brightIdeas.UpdateIdea(id, "", 10, "", "");
            }
            Response.Redirect(Request.RawUrl);
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            if (status == "2")
            {
                brightIdeas.UpdateIdea(id, "", 8, "", "");
            }
            if (status == "6")
            {
                brightIdeas.UpdateIdea(id, "", 9, "", "");
            }
            Response.Redirect(Request.RawUrl);
        }

        protected void btnCurrentState_Click(object sender, EventArgs e)
        {
            DownloadFile(1);
        }
        protected void btnProposedImprovement_Click(object sender, EventArgs e)
        {
            DownloadFile(2);
        }

        public void DownloadFile(int type)
        {
            DataSet ds = new DataSet();
            ds = brightIdeas.DownloadFile(id, type);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = ds.Tables[0].Rows[0]["AttachmentContentType"].ToString();
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + ds.Tables[0].Rows[0]["AttachmentFileNm"].ToString());
            Response.BinaryWrite((byte[])ds.Tables[0].Rows[0]["Data"]);
            Response.Flush();
            Response.End();
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect("MasterDataView.aspx");
        }
    }
}