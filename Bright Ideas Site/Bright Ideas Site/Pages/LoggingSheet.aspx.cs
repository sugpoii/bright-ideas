﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using BLL;
using System.Collections;
using System.IO;
using System.Windows.Input;

namespace Bright_Ideas_Site.Pages
{
    public partial class LoggingView : System.Web.UI.Page
    {
        Bright_Ideas brightIdeas = new Bright_Ideas();

        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateRequestorsInformation();
            PopulateBusinessUnitDropdown();
            PopulateIdeaType();
        }

        public void PopulateIdeaType()
        {
            ArrayList list = new ArrayList();
            DataSet ds = brightIdeas.GetIdeaTypes();
            ddlIdeaType.DataSource = ds.Tables[0];
            ddlIdeaType.DataTextField = "DecodeTxt";
            ddlIdeaType.DataValueField = "CodeTxt";
            ddlIdeaType.DataBind();
        }

        public void PopulateRequestorsInformation()
        {
            string name = /*Environment.UserName;*/ "jabercasio";
            DataSet ds = new DataSet();
            ds = brightIdeas.GetRequestorInformation(name);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtRequestorName.Text = ds.Tables[0].Rows[0]["FULL_NAME"].ToString();
                txtPosition.Text = ds.Tables[0].Rows[0]["POSITION"].ToString();
                txtRankLevel.Text = ds.Tables[0].Rows[0]["RANK_LEVEL"].ToString();
                txtBusinessUnit.Text = ds.Tables[0].Rows[0]["BU"].ToString();
            }
            string lineManager = ds.Tables[0].Rows[0]["IMMEDIATE_SUPERVISOR_USERNAME"].ToString();
            ds = brightIdeas.GetRequestorInformation(lineManager);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtLineManager.Text = ds.Tables[0].Rows[0]["FULL_NAME"].ToString();
            }
            string businessHead = ds.Tables[0].Rows[0]["IMMEDIATE_SUPERVISOR_USERNAME"].ToString();
            ds = brightIdeas.GetRequestorInformation(businessHead);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtBusinessHead.Text = ds.Tables[0].Rows[0]["FULL_NAME"].ToString();
            }
            ds = brightIdeas.GetAceChampion(ds.Tables[0].Rows[0]["BU"].ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                string names = "";
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    names = names + item["FULL_NAME"].ToString() + "; ";
                }
                txtACEChampion.Text = names;
            }
        }
        public void PopulateBusinessUnitDropdown()
        {
            DataSet ds = brightIdeas.GetBusinessUnitList();
            ddlBusinessUnit.DataSource = ds.Tables[0];
            ddlBusinessUnit.DataTextField = "BU";
            ddlBusinessUnit.DataValueField = "BU";
            ddlBusinessUnit.DataBind();
        }

        protected void ddlBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateACEChampion();
            PopulateProjectOwner();
        }

        public void PopulateACEChampion()
        {
            ArrayList list = new ArrayList();
            DataSet ds = brightIdeas.GetAceChampion(ddlBusinessUnit.Text);
            ddlACEChampion.DataSource = ds.Tables[0];
            ddlACEChampion.DataTextField = "FULL_NAME";
            ddlACEChampion.DataValueField = "USER_NAME";
            ddlACEChampion.DataBind();
            ddlACEChampion.Items.Insert(0, new ListItem("Select ACE Champion", ""));
        }

        public void PopulateProjectOwner()
        {
            ArrayList list = new ArrayList();
            DataSet ds = brightIdeas.GetProjectOwner(ddlBusinessUnit.Text);
            ddlProjectOwner.DataSource = ds.Tables[0];
            ddlProjectOwner.DataTextField = "FULL_NAME";
            ddlProjectOwner.DataValueField = "USER_NAME";
            ddlProjectOwner.DataBind();
            ddlProjectOwner.Items.Insert(0, new ListItem("Select Project Owner", ""));
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DataSet ds = new DataSet();
                string id = "";
                ds = brightIdeas.SaveIdea( /*Environment.UserName;*/ "jabercasio",
                txtIdeaTitle.Text, ddlIdeaType.SelectedValue,
                txtEstimatedBenefits.Text, ddlACEChampion.SelectedValue,
                ddlProjectOwner.Text, 1, txtEstimatedCompletionDate.Text);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    id = ds.Tables[0].Rows[0]["id"].ToString();
                    string filename = Path.GetFileName(fuploadCurrentState.PostedFile.FileName);
                    string contentType = fuploadCurrentState.PostedFile.ContentType;
                    using (Stream fs = fuploadCurrentState.PostedFile.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            brightIdeas.UploadAttachment(Convert.ToInt32(id), filename, contentType, "1", bytes);
                        }
                    }
                    filename = Path.GetFileName(fuploadProposedImprovement.PostedFile.FileName);
                    contentType = fuploadProposedImprovement.PostedFile.ContentType;
                    using (Stream fs = fuploadProposedImprovement.PostedFile.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            brightIdeas.UploadAttachment(Convert.ToInt32(id), filename, contentType, "2", bytes);
                        }
                    }
                    Page.Response.Redirect("IdeaDetails.aspx?id=" + id);
                }
            }
        }

        protected void ddlACEChampion_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateSupervisors();
        }

        public void PopulateSupervisors()
        {
            DataSet ds = new DataSet();
            ds = brightIdeas.SelectSupervisors(ddlACEChampion.SelectedValue);
            txtChampionLineManager.Text = ds.Tables[0].Rows[0]["LineManager"].ToString();
            txtChampionBusinessHead.Text = ds.Tables[0].Rows[0]["BUHead"].ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect("MasterDataView.aspx");
        }
    }
}